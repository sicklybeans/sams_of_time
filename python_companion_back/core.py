
class Error(Exception):
  msg: str
  def __init__(self, msg: str) -> None:
    super().__init__(msg)
    self.msg = msg


class MissingResourceError(Error):
  pass

class ParseException(Error):
  pass
