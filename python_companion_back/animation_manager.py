from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

from typing import Dict, cast

from core import MissingResourceError
from sprite import Sprite
from sprite_manager import SpriteManager
from animation import Animation, RegAnimation, DirAnimation
from animation_loader import RData


class AnimationManager():
  sprite_manager: SpriteManager
  animations: Dict[str, Animation]

  def __init__(self, sprite_manager: SpriteManager) -> None:
    self.sprite_manager = sprite_manager
    self.animations = {}
    self._add_resource_dir('../assets/animations')

  def _add_resource_dir(self, resource_dir: str) -> None:
    raw = RData.load_resource(resource_dir)
    raw.consume(self.sprite_manager, self.animations)

  def report_info(self) -> None:
    reg = [a for _, a in self.animations.items() if isinstance(a, RegAnimation)]
    direc = [a for _, a in self.animations.items() if isinstance(a, DirAnimation)]
    print('AnimationManager has %d regular and %d directional animations' % (len(reg), len(direc)))

if __name__ == '__main__':
  sm = SpriteManager()
  am = AnimationManager(sm)
  am.report_info()
