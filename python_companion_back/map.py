from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

import cairo

from sprite import Sprite
from core import ParseException

class Map():
  x1: float
  y1: float
  tile_map: Dict[int, Sprite]
  tile_width: int
  tile_height: int
  background: List[List[List[int]]]

  def __init__(self, x1: float, y1: float, tile_map: Dict[int, Sprite], background: List[List[List[int]]]) -> None:
    self.x1 = x1
    self.y1 = y1
    self.tile_map = tile_map
    self.background = background
    self._check_integrity()
    key = list(self.tile_map.keys())[0]
    self.tile_width, self.tile_height = self.tile_map[key].get_size()

  def get_tile_map(self) -> Dict[int, Sprite]:
    return self.tile_map

  def _check_integrity(self):
    """Ensures map has no obvious errors in it."""

    # 1. Check that all rows in map have same length
    nrows = len(self.background)
    ncols = len(self.background[0])
    for i, row in enumerate(self.background):
      if len(row) != ncols:
        raise ParseException('Row %d has different length %d != %d' % (i, len(row), ncols))

    # 2. Check that all entries in map are listed in the tile_map
    for i in range(nrows):
      for j in range(ncols):
        for id in self.background[i][j]:
          if id not in self.tile_map:
            raise ParseException('Map position (i,j)=(%d, %d) contains unknown tile id %d' % (i, j, id))

    # 3. Check that all tiles used in map have same dimensions
    sprite_width, sprite_height = None, None
    for id, sprite in self.tile_map.items():
      if sprite_width is None:
        sprite_width, sprite_height = sprite.get_size()
      else:
        w, h = sprite.get_size()
        if w != sprite_width:
          raise ParseException('Tile id %d in tile map has unexpected width %d' % (id, w))
        if h != sprite_height:
          raise ParseException('Tile id %d in tile map has unexpected height %d' % (id, h))

  def get_tile_size(self):
    return self.tile_width, self.tile_height

  def get_pixel_size(self):
    nrows = len(self.background)
    ncols = len(self.background[0])
    return self.tile_width * ncols, self.tile_height * nrows

  def get_sprites_at(self, xind: int, yind: int) -> List[Sprite]:
    nrows = len(self.background)
    ncols = len(self.background[0])
    if xind < 0 or yind < 0:
      raise Exception('Position out of bounds: %d, %d' % (xind, yind))
    elif xind >= ncols or yind >= nrows:
      raise Exception('Position out of bounds: %d, %d' % (xind, yind))
    return [self.tile_map[id] for id in self.background[yind][xind]]

  def draw_layer(self, cr: cairo.Context, layer: int) -> bool:
    has_tile = False
    tw, th = self.get_tile_size()
    y = 0
    for row in self.background:
      x = 0
      for layers in row:
        if layer < len(layers):
          has_tile = True
          Gdk.cairo_set_source_pixbuf(cr, self.tile_map[layers[layer]].get_image(), x, y)
          cr.paint()
        x += tw
      y += th
    return has_tile

  def draw(self, cr: cairo.Context) -> None:
    layer = 0
    while self.draw_layer(cr, layer):
      layer += 1

  # def get_image(self) -> GdkPixbuf.Pixbuf:
  #   w, h = self.get_pixel_size()
  #   nrows = len(self.background)
  #   ncols = len(self.background[0])
  #   image = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, w, h)

  #   for i in range(nrows):
  #     y = self.tile_height * i
  #     for j in range(ncols):
  #       x = self.tile_width * j
  #       for id in self.background[i][j]:
  #         self.tile_map[id].draw_at(image, x, y)

  #   return image
