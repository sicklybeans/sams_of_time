from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

from typing import Dict, cast

from core import MissingResourceError
from sprite import Sprite
from sprite_loader import RData


class SpriteManager():
  sprites: Dict[str, Sprite]

  def __init__(self) -> None:
    self.sprites = {}
    self._add_resource_dir('../assets/tilesets')
    self._add_resource_dir('../assets/sprites')

  def _add_resource_dir(self, resource_dir: str) -> None:
    RData.load_resource(resource_dir).consume(self.sprites)

  def get_sprite(self, key: str) -> Sprite:
    if key in self.sprites:
      return self.sprites[key]
    else:
      raise MissingResourceError('No such sprite: %s' % key)

  def report_info(self) -> None:
    print('SpriteManager has %d sprites' % len(self.sprites))

if __name__ == '__main__':

  sm = SpriteManager()
  sm.report_info()
