from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

import os, json
from typing import Dict, Any, cast

class ParseException(Exception):
  msg: str
  def __init__(self, msg) -> None:
    super().__init__(msg)
    self.msg = msg

class Region():
  x: int
  y: int
  width: int
  height: int

  def __init__(self, x: int, y: int, width: int, height: int):
    self.x = x
    self.y = y
    self.width = width
    self.height = height

class Sprite():
  width: int
  height: int
  image: GdkPixbuf.Pixbuf

  def __init__(self, image: GdkPixbuf.Pixbuf) -> None:
    self.image = image
    self.width = image.get_width()
    self.height = image.get_height()

class RawData():
  parent_dir: str
  spritesheets: List[RawData.RawSpriteSheet]

  def __init__(self, parent_dir: str, json_dict: Dict[str, Any]) -> None:
    self.parent_dir = parent_dir
    self.spritesheets = list(map(RawData.SpriteSheet, json_dict['spritesheets']))

  class SpriteSheet():
    filename: str
    rows: int
    cols: int
    assets: List[RawData.SpriteSheet.Sprite]

    def __init__(self, json_dict: Dict[str, Any]) -> None:
      self.filename = json_dict['filename']
      self.rows = cast(int, json_dict['rows'])
      self.cols = cast(int, json_dict['cols'])
      self.assets = list(map(RawData.SpriteSheet.Sprite, json_dict['assets']))

    class Sprite():
      key: str
      x_index: int
      y_index: int
      width: int
      height: int

      def __init__(self, json_dict: Dict[str, Any]) -> None:
        self.key = json_dict['key']
        self.x_index = json_dict['x_index']
        self.y_index = json_dict['y_index']
        self.width = json_dict['width']
        self.height = json_dict['height']

      def _consume(self, sprites: Dict[str, Sprite], image: GdkPixbuf.Pixbuf, tile_width: int, tile_height: int) -> Sprite:
        if self.key in sprites:
          raise ParseException('Duplicate sprite key: %s' % self.key)

        x1, y1 = self.x_index * tile_width, self.y_index * tile_height
        w, h = self.width * tile_width, self.height * tile_height
        if (x1 + w) > image.get_width():
          raise ParseException('Sprite target for %s exceeds width' % self.key)
        if (y1 + h) > image.get_height():
          raise ParseException('Sprite target for %s exceeds height' % self.key)

        sprites[self.key] = Sprite(image.new_subpixbuf(x1, y1, w, h))
        del self

    def __repr__(self) -> str:
      return self.filename

    def _consume(self, sprites: Dict[str, Sprite], parent_dir: str) -> None:
      try:
        image_file = os.path.join(parent_dir, self.filename)
        if not os.path.exists(image_file):
          raise ParseException('Image file not found')
        image = GdkPixbuf.Pixbuf.new_from_file(image_file)

        # Convert spritesheet into segments based on number of rows and cols
        w, h = image.get_width(), image.get_height()
        if w % self.cols != 0:
          raise ParseException('Image width (%d) not divisible by number of cols (%d)' % (w, self.cols))
        if h % self.rows != 0:
          raise ParseException('Image height (%d) not divisible by number of rows (%d)' % (h, self.rows))
        tile_width = int(w / self.cols)
        tile_height = int(h / self.rows)

        for s in self.assets:
          s._consume(sprites, image, tile_width, tile_height)
      except ParseException as ex:
        raise ParseException('Error in spritesheet %s: %s' % (self, ex.msg))

      del self

  def __repr__(self) -> str:
    return self.parent_dir

  @staticmethod
  def load_resource_dir(resource_dir: str) -> RawData:
    info_file = os.path.join(resource_dir, 'info.json')
    d = json.loads(open(info_file).read())
    return RawData(resource_dir, d)

  def consume(self, sprites: Dict[str, Sprite]) -> None:
    try:
      for ss in self.spritesheets:
        ss._consume(sprites, self.parent_dir)

    except ParseException as ex:
      raise ParseException('Error in resource %s: %s' % (self, ex.msg))

    del self

class SpriteManager():
  _sprites: Dict[str, Sprite]
  _sprite_sheets: List[GdkPixbuf.Pixbuf]

  def __init__(self) -> None:
    f = '../assets/sprites/info.json'
    d = json.loads(open(f, 'r').read())
    self._sprites = {}
    self._sprite_sheets = []
    for sheet in d['spritesheets']:
      self._process_sprite_sheet(sheet)

  def _process_sprite_sheet(d) -> None:
    ind = len(self._sprite_sheets)

    rows = int(d['rows'])
    cols = int(d['cols'])
    filename = d['filename']


