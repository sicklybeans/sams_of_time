from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

import os, json
from typing import ClassVar, Dict, Any, List, cast

from core import ParseException
from loaders import ResourceLoader
from sprite import Sprite

class RData(ResourceLoader):
  spritesheets: List[RData.RSpriteSheet]

  def __init__(self, resource_dir: str, json_dict: Dict[str, Any]) -> None:
    super().__init__(resource_dir, json_dict)
    self.resource_dir = resource_dir
    self.spritesheets = list(map(RData.RSpriteSheet, json_dict['spritesheets']))

  class RSpriteSheet():
    filename: str
    rows: int
    cols: int
    assets: List[RData.RSpriteSheet.RSprite]

    def __init__(self, json_dict: Dict[str, Any]) -> None:
      self.filename = json_dict['filename']
      self.rows = cast(int, json_dict['rows'])
      self.cols = cast(int, json_dict['cols'])
      self.assets = list(map(RData.RSpriteSheet.RSprite, json_dict['assets']))

    class RSprite():
      key: str
      x_index: int
      y_index: int
      width: int
      height: int

      def __init__(self, json_dict: Dict[str, Any]) -> None:
        self.key = json_dict['key']
        self.x_index = json_dict['x_index']
        self.y_index = json_dict['y_index']
        self.width = json_dict['width']
        self.height = json_dict['height']

      def _consume(self, sprites: Dict[str, Sprite], image: GdkPixbuf.Pixbuf, tile_width: int, tile_height: int) -> None:
        if self.key in sprites:
          raise ParseException('Duplicate sprite key: %s' % self.key)

        x1, y1 = self.x_index * tile_width, self.y_index * tile_height
        w, h = self.width * tile_width, self.height * tile_height
        if (x1 + w) > image.get_width():
          raise ParseException('Sprite target for %s exceeds width' % self.key)
        if (y1 + h) > image.get_height():
          raise ParseException('Sprite target for %s exceeds height' % self.key)

        sprites[self.key] = Sprite(self.key, image.new_subpixbuf(x1, y1, w, h))

    def __repr__(self) -> str:
      return self.filename

    def _consume(self, resource_dir: str, sprites: Dict[str, Sprite]) -> None:
      try:
        image_file = os.path.join(resource_dir, self.filename)
        if not os.path.exists(image_file):
          raise ParseException('Image file not found')
        image = GdkPixbuf.Pixbuf.new_from_file(image_file)

        # Convert spritesheet into segments based on number of rows and cols
        w, h = image.get_width(), image.get_height()
        if w % self.cols != 0:
          raise ParseException('Image width (%d) not divisible by number of cols (%d)' % (w, self.cols))
        if h % self.rows != 0:
          raise ParseException('Image height (%d) not divisible by number of rows (%d)' % (h, self.rows))
        tile_width = int(w / self.cols)
        tile_height = int(h / self.rows)

        for s in self.assets:
          s._consume(sprites, image, tile_width, tile_height)
      except ParseException as ex:
        raise ParseException('Error in spritesheet %s: %s' % (self, ex.msg))

  def consume(self, sprites: Dict[str, Sprite]) -> None:
    super().consume()
    try:
      for ss in self.spritesheets:
        ss._consume(self.resource_dir, sprites)

    except ParseException as ex:
      raise ParseException('Error in resource %s: %s' % (self, ex.msg))
