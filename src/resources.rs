use std::collections::HashMap;
use sdl2::render::Texture;
use std::path::Path;
use sdl2::render::TextureCreator;
use sdl2::video::WindowContext;
use sdl2::render::Canvas;
use sdl2::video::Window;
use sdl2::image::LoadTexture;
use sdl2::rect::Rect;
use sdl2::pixels::PixelFormatEnum;
use serde::{Deserialize, Serialize};

use crate::components::*;


pub fn test() {
  let sdl = sdl2::init().unwrap();

  let canvas = sdl.video()
    .expect("Failed to get video subsystem")
    .window("Game", 900, 700)
    .resizable()
    .build()
    .expect("Failed to get window")
    .into_canvas()
    .build()
    .expect("Failed to build canvas for window");
  let mut tm = TextureManager::new(&canvas);
  tm.add_texture_pack("assets/sprites").unwrap();
  tm.add_texture_pack("assets/tilesets").unwrap();
}

/// Allows loading and getting texture data for named Sprite objects provided through JSON.
pub struct TextureManager {
  texture_creator: TextureCreator<WindowContext>,
  texture_sheets: Vec<Texture>,
  texture_map: HashMap<String, Sprite>,
  animation_map: HashMap<String, SimpleAnimation>,
  directional_animation_map: HashMap<String, DirectionalAnimation>
}

#[derive(Serialize, Deserialize)]
struct TexturePackData {
  spritesheets: Vec<SpritesheetData>
}

#[derive(Serialize, Deserialize)]
struct SpritesheetData {
  filename: String,
  rows: u32, // number of rows of sprites in sheet,
  cols: u32, // number of cols of sprites in sheet,
  assets: Vec<SpriteData>
}

#[derive(Serialize, Deserialize)]
struct SpriteData {
  key: String, // Unique name referring to tile/sprite/whatever.
  x_index: usize, // index on spritesheet (in tile units)
  y_index: usize, // index on spritesheet (in tile units)
  width: usize, // width on spritesheet (in tile units)
  height: usize // height on spritesheet (in tile units)
}

#[derive(Serialize, Deserialize)]
struct AnimationPackData {
  animations: Vec<AnimationData>,
  directional_animations: Vec<DirectionalAnimationData>
}

#[derive(Serialize, Deserialize)]
struct AnimationData {
  key: String,
  sprites: Vec<String>
}

#[derive(Serialize, Deserialize)]
struct DirectionalAnimationData {
  key: String,
  e: String,
  // se: String,
  s: String,
  // sw: String,
  w: String,
  // nw: String,
  n: String,
  // ne: String,
}

// #[derive(Serialize, Deserialize)]
// struct AnimationPackData {
//   animations: Vec<AnimationData>,
//   movement_animations: Vec<DirectionalAnimationData>
// }

// #[derive(Serialize, Deserialize)]
// struct AnimationData {
//   key: String,
//   sprites: Vec<String>
// }

// #[derive(Serialize, Deserialize)]
// struct DirectionalAnimationData {
//   key: String,
//   up: String,
//   down: String,
//   left: String,
//   right: String
// }

impl TexturePackData {
  fn load_from_file<P: AsRef<Path>>(resource_file: P) -> Result<TexturePackData, String> {
    let contents = std::fs::read_to_string(resource_file)
      .map_err(|e| e.to_string())?;
    serde_json::from_str(&contents)
      .map_err(|e| e.to_string())
  }

  fn load_from_directory<P: AsRef<Path>>(resource_dir: P) -> Result<TexturePackData, String> {
    let info_path = resource_dir.as_ref().join("info.json");
    TexturePackData::load_from_file(info_path)
  }
}

impl AnimationPackData {
  fn load_from_file<P: AsRef<Path>>(resource_file: P) -> Result<AnimationPackData, String> {
    let contents = std::fs::read_to_string(resource_file)
      .map_err(|e| e.to_string())?;
    serde_json::from_str(&contents)
      .map_err(|e| e.to_string())
  }

  fn load_from_directory<P: AsRef<Path>>(resource_dir: P) -> Result<AnimationPackData, String> {
    let info_path = resource_dir.as_ref().join("info.json");
    AnimationPackData::load_from_file(info_path)
  }
}

impl TextureManager {

  pub fn new(canvas: &Canvas<Window>) -> TextureManager {
    TextureManager{
      texture_creator: canvas.texture_creator(),
      texture_sheets: Vec::new(),
      texture_map: HashMap::new(),
      animation_map: HashMap::new(),
      directional_animation_map: HashMap::new()
    }
  }

  pub fn print_loaded_content(&self) {
    println!("Number loaded textures  : {}", self.texture_map.len());
    println!("Number loaded animations: {}", self.animation_map.len());
    println!("Movement animation map  : {}", self.directional_animation_map.len());
    for (key, _) in self.directional_animation_map.iter() {
      println!("  key: {}", key);
    }
  }

  pub fn add_texture_pack_file(&mut self, pack_dir: &str, info_file: &str) -> Result<(), String> {
    let info_path = format!("{}/{}", pack_dir, info_file);
    let tpack_info = TexturePackData::load_from_file(&info_path)?;
    self.process_texture_pack(pack_dir.as_ref(), tpack_info)
  }

  pub fn add_animation_pack_file(&mut self, pack_dir: &str, info_file: &str) -> Result<(), String> {
    let info_path = format!("{}/{}", pack_dir, info_file);
    let tpack_info = AnimationPackData::load_from_file(&info_path)?;
    self.process_animation_pack(tpack_info)
  }

  pub fn add_texture_pack<P>(&mut self, pack_dir: P) -> Result<(), String> where P: AsRef<Path> {
    let tpack_info = TexturePackData::load_from_directory(&pack_dir)?;
    self.process_texture_pack(pack_dir.as_ref(), tpack_info)
  }

  pub fn add_animation_pack<P>(&mut self, pack_dir: P) -> Result<(), String> where P: AsRef<Path> {
    let tpack_info = AnimationPackData::load_from_directory(&pack_dir)?;
    self.process_animation_pack(tpack_info)
  }

  pub fn get_default_sprite(&self) -> Sprite {
    if let Some(sprite) = self.texture_map.get("empty-sprite") {
      sprite.clone()
    } else {
      panic!("Could not load default sprite")
    }
  }

  /// Creates new Sprite component corresponding to the given key.
  /// This method is slow - creates new Sprite and uses HashMap lookup. Only use this when loading
  /// assets and doing pre-rendering. Never use this in an actual rendering loop.
  pub fn get_sprite(&self, key: &str) -> Result<Sprite, String> {
    if let Some(sprite) = self.texture_map.get(key) {
      Ok(sprite.clone())
    } else {
      Err(String::from("No such sprite ") + key)
    }
  }

  pub fn get_animation(&self, key: &str) -> Result<SimpleAnimation, String> {
    if let Some(ani) = self.animation_map.get(key) {
      Ok(ani.clone())
    } else {
      Err(String::from("No such animation ") + key)
    }
  }

  pub fn get_directional_animation(&self, key: &str) -> Result<DirectionalAnimation, String> {
    if let Some(ani) = self.directional_animation_map.get(key) {
      Ok(ani.clone())
    } else {
      Err(String::from("No such movement animation ") + key)
    }
  }

  pub fn get_sprite_texture(&self, texture_index: usize) -> &Texture {
    &self.texture_sheets[texture_index]
  }

  pub fn make_new_texture(&self, width: u32, height: u32) -> Result<Texture, String> {
    self.texture_creator.create_texture_target(PixelFormatEnum::RGBA32, width, height)
      .map_err(|e| e.to_string())
  }

  fn add_texture_sheet<P: AsRef<Path>>(&mut self, spritesheet_file: P) -> Result<usize, String> {
    let texture = self.texture_creator.load_texture(spritesheet_file)?;
    self.texture_sheets.push(texture);
    Ok(self.texture_sheets.len() - 1)
  }

  fn add_sprite(&mut self, tsheet_index: usize, col_size: usize, row_size: usize, texture_info: SpriteData) -> Result<(), String> {
    // Compute region of sprite sheet occupied by this sprite.
    let region = Rect::new(
      (texture_info.x_index * col_size) as i32,
      (texture_info.y_index * row_size) as i32,
      (texture_info.width * col_size) as u32,
      (texture_info.height * row_size) as u32);

    let sprite = Sprite {
      texture_index: tsheet_index,
      region: region
    };

    // Insert sprite into map and make sure it didnt overwrite existing entry
    if let Some(_old_sprite) = self.texture_map.insert(texture_info.key.clone(), sprite) {
      Err(format!("Duplicate texture key: {}", texture_info.key.clone()))
    } else {
      Ok(())
    }
  }

  fn process_texture_pack(&mut self, pack_dir: &Path, mut tpack_info: TexturePackData) -> Result<(), String> {
    // Add each sprite sheet into the texture manager
    while let Some(mut tsheet_info) = tpack_info.spritesheets.pop() {
      let tsheet_filename = pack_dir.join(&tsheet_info.filename);
      let tsheet_index = self.add_texture_sheet(tsheet_filename)?;
      let texture_info = self.texture_sheets[tsheet_index].query();
      let col_size = (texture_info.width / tsheet_info.cols) as usize;
      let row_size = (texture_info.height / tsheet_info.rows) as usize;
      while let Some(t_info) = tsheet_info.assets.pop() {
        self.add_sprite(tsheet_index, col_size, row_size, t_info)?;
      }
    }
    Ok(())
  }

  fn add_animation(&mut self, mut ani_info: AnimationData) -> Result<(), String> {
    println!("Adding animation {}", ani_info.key);
    let mut sprites = Vec::new();
    while let Some(sprite_key) = ani_info.sprites.pop() {
      let sprite = self.get_sprite(&sprite_key)
        .map_err(|_| format!("Cannot load animation {}: sprite not found: {}", ani_info.key, sprite_key))?;
      sprites.push(sprite);
    }
    sprites.reverse();
    let ani = SimpleAnimation::new(sprites);
    if let Some(_) = self.animation_map.insert(ani_info.key.clone(), ani) {
      Err(format!("Duplicate animation key: {}", ani_info.key.clone()))
    } else {
      Ok(())
    }
  }
  fn add_movement_animation(&mut self, dir_ani_info: DirectionalAnimationData) -> Result<(), String> {
    let up = self.get_animation(&dir_ani_info.n)
      .expect(&format!("movement animation {} contained invalid animation {}", dir_ani_info.key, &dir_ani_info.n));
    let down = self.get_animation(&dir_ani_info.s)
      .expect(&format!("movement animation {} contained invalid animation {}", dir_ani_info.key, &dir_ani_info.s));
    let left = self.get_animation(&dir_ani_info.w)
      .expect(&format!("movement animation {} contained invalid animation {}", dir_ani_info.key, &dir_ani_info.w));
    let right = self.get_animation(&dir_ani_info.e)
      .expect(&format!("movement animation {} contained invalid animation {}", dir_ani_info.key, &dir_ani_info.e));

    let ani = DirectionalAnimation::new(&up, &down, &left, &right)
      .map_err(|msg| {format!("Error in movement animation '{}': {}", dir_ani_info.key, msg)})?;
    if let Some(_) = self.directional_animation_map.insert(dir_ani_info.key.clone(), ani) {
      Err(format!("Duplicate movement animation key: {}", dir_ani_info.key.clone()))
    } else {
      Ok(())
    }
  }
  fn process_animation_pack(&mut self, mut anipack_info: AnimationPackData) -> Result<(), String> {
    while let Some(ani_info) = anipack_info.animations.pop() {
      self.add_animation(ani_info)?;
    }
    while let Some(ani_info) = anipack_info.directional_animations.pop() {
      self.add_movement_animation(ani_info)?;
    }
    Ok(())
  }
}
