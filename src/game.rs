use sdl2::render::Canvas;
use sdl2::pixels::Color;
use sdl2::video::Window;
use specs::prelude::*;
use std::time::{Instant, Duration};

use crate::components::*;
use crate::graphics;
use crate::map::Map;
use crate::resources::TextureManager;
use crate::loader::EntityManager;
use crate::systems::*;

pub struct Game<'a, 'b> {
  pub canvas: Canvas<Window>,
  pub config: Config,
  pub map: Option<Map>,
  pub world: World,
  pub input_dispatcher: Dispatcher<'a, 'b>,
  pub main_dispatcher: Dispatcher<'a, 'b>,
  pub texture_manager: TextureManager,
  pub entity_manager: EntityManager
}

/// Miscellaneous global parameters.
pub struct Config {
  /// Sleep time at end of each game loop, will eventually be removed in favor of a vsync-based
  /// solution, or at least one that takes into account the run time of the thread.
  pub sleep_time: Duration,
  /// Logical size of each game tile - should probably be 1.0 (as in minecraft).
  pub logical_tile_size: f32,
  /// Number of pixels for each logical unit in game (i.e. number of pixels for each tile).
  pub pixels_per_unit: f32,
  /// Exact number of pixels for each tile
  pub physical_tile_size: u32,
  /// Background color to cover world with
  pub background_color: Color,
}

/* Implementations */
impl Config {
  pub fn new() -> Config {
    Config {
      sleep_time: Duration::new(0, 1_000_000_000u32 / 60),
      logical_tile_size: 1.0,
      pixels_per_unit: 32.0,
      physical_tile_size: 32,
      background_color: Color::RGB(0, 0, 0)
    }
  }
}

impl<'a, 'b> Game<'a, 'b> {
  pub fn new() -> Result<Game<'a, 'b>, String> {
    let mut sdl = sdl2::init().unwrap();

    let canvas = sdl.video()
      .expect("Failed to get video subsystem")
      .window("Game", 900, 700)
      .resizable()
      .build()
      .expect("Failed to get window")
      .into_canvas()
      .build()
      .expect("Failed to build canvas for window");

    let config = Config::new();
    let mut texture_manager = TextureManager::new(&canvas);
    texture_manager.add_texture_pack("assets/sprites")?;
    texture_manager.add_texture_pack("assets/tilesets")?;
    texture_manager.add_animation_pack("assets/animations")?;
    texture_manager.print_loaded_content();

    let entity_manager = EntityManager::new(&mut texture_manager)?;
    entity_manager.print_loaded_content();

    println!("Finished loading assets");

    let world = get_world();
    let input_dispatcher =
    {
      let mut builder = DispatcherBuilder::new()
        .with_thread_local(KeyboardSystem::new(&mut sdl));
      if let Ok(joystick_system) = JoystickSystem::new(&mut sdl) {
        builder = builder.with_thread_local(joystick_system);
      }
      builder.build()
    };

    let main_dispatcher = DispatcherBuilder::new()
      .with(AiSystem::new(), "ai_system", &[]) // determines move behavior
      .with(ActionQueueSystem, "action_queue_system", &["ai_system"]) // handles queued action events
      .with(MovementSystem, "move_system", &["action_queue_system"]) // proposes moves
      .with(AnimationSystem, "animation_system", &["ai_system"]) // sets sprites based on action state (set by input/ai)
      .with(CollisionDetectionSystem, "collision_system", &["move_system"])
      .with(UpdatePositionSystem, "update_pos_system", &["move_system", "collision_system"])
      .with(ActionUpdateSystem, "action_update_system", &["animation_system"]) // updates state of action components
      .with(CleanupSystem, "cleanup_system", &["update_pos_system"])
      .build();

    Ok(Game {
      canvas, config, map: None, world, input_dispatcher, main_dispatcher, texture_manager, entity_manager
    })
  }

  pub fn run(&mut self) -> Result<(), String> {


    self.canvas.present();

    let loop_time = self.config.sleep_time;
    let mut total_run_time = Duration::new(0, 0);
    let mut total_sleep_time = Duration::new(0, 0);
    let mut loop_count = 0;

    let mut loop_start = Instant::now();
    let total_timer = Instant::now();
    loop {
      self.input_dispatcher.dispatch_thread_local(&mut self.world);
      self.main_dispatcher.dispatch(&mut self.world);
      graphics::render(
        &self.config,
        &mut self.canvas,
        &mut self.map,
        &(self.texture_manager),
        self.world.system_data()
      )?;

      if self.world.fetch::<GameState>().is_quit {
        break;
      }

      let elapsed_time = loop_start.elapsed();

      if let Some(sleep_time) = loop_time.checked_sub(elapsed_time) {
        // TODO: make config read only and copy it so we don't have to load reference every time.
        std::thread::sleep(sleep_time);
        loop_count += 1;
        total_run_time += elapsed_time;
        total_sleep_time += sleep_time;
      } else {
        println!("Cant keep up!");
      }
      loop_start = Instant::now();

      // This is not ideal. We should only call this when necessary. I also think storing everything
      // in a global Game object is a terrible idea because of the implications for the borrow
      // checker. However, it will have to do for now.
      self.world.maintain();


    }
    let total_time = total_timer.elapsed();

    println!("Total number loops: {}", loop_count);
    println!("Total time        : {}", (total_time.as_nanos() as f64) / 1_000_000_000.0);
    println!("Avg framerate     : {}", ((loop_count as f64) / (total_time.as_nanos() as f64) * 1_000_000_000.0));

    println!("Finishing up");
    let run_time = total_run_time.as_nanos() as f64;
    let slp_time = total_sleep_time.as_nanos() as f64;
    let run_avg = run_time / (loop_count as f64);
    let slp_avg = slp_time / (loop_count as f64);
    let target_avg = loop_time.as_nanos() as f64;

    println!("number of loops: {}", loop_count);
    println!("ns per update: {}", run_avg);
    println!("ns per sleep : {}", slp_avg);
    println!("% budget used: {}", run_avg / target_avg);

    Ok(())
  }

  /// For testing only!
  pub fn load_map(&mut self, map_name: &str) -> Result<(), String> {
    let map = Map::load_map(self, map_name);
    self.map = Some(map);
    Ok(())
  }
}

