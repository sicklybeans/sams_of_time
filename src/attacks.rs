#![allow(dead_code)]
use specs::prelude::*;
use crate::components::*;


// pub enum SwordAttack {
//   ArcAttack(Box<ArcAttackData>)
// }

pub trait AttackData : Send + Sync {
  fn update(&mut self);
  fn get_raw_dmg(&self) -> f32;
  fn intersects_with(&self, bound: &Bound) -> bool;
  fn get_sprite(&self, direction: Direction) -> Sprite;
}

pub struct Attack {
  pub frame_length: u32,
  pub ellapsed_frames: u32,
  pub src_entity: Option<Entity>,
  pub data: Box<dyn AttackData>
}

impl Component for Attack {
  type Storage = DenseVecStorage<Self>;
}

impl Attack {
  /// Updates state of the attack including the animation and returns true if attack is not
  /// finished.
  ///
  /// This should be called at the end of every frame after the graphics are drawn.
  pub fn update(&mut self) -> bool {
    self.ellapsed_frames += 1;

    self.data.update();

    if self.ellapsed_frames >= self.frame_length {
      if self.ellapsed_frames == self.frame_length {
        false
      } else {
        panic!("Should never happen!")
      }
    } else {
      true
    }
  }
  pub fn get_sprite(&self, direction: Direction) -> Sprite {
    self.data.get_sprite(direction)
  }
  pub fn intersects_with(&self, bound: &Bound) -> bool {
    self.data.intersects_with(bound)
  }
}

pub struct AttackHitSystem;

impl<'a> System<'a> for AttackHitSystem {
  type SystemData = (
    Entities<'a>,
    ReadStorage<'a, Bound>,
    WriteStorage<'a, Health>,
    ReadStorage<'a, Attack>
  );

  fn run(&mut self, (entities, bound, mut health, attack): Self::SystemData) {
    // Check for collisions of (health, subbound) against (attack)
    for (dst_entity, bound, health) in (&*entities, &bound, &mut health).join() {
      for attack in attack.join() {

        if let Some(src) = attack.src_entity {
          if src == dst_entity {
            continue;
          }
        }

        if !attack.intersects_with(bound) {
          continue;
        }

        health.apply_dmg(attack.data.get_raw_dmg());
      }
    }
  }
}

pub struct AttackCleanupSystem;

impl<'a> System<'a> for AttackCleanupSystem {
  type SystemData = (
    Entities<'a>,
    WriteStorage<'a, Attack>,
  );

  fn run(&mut self, (entities, mut attacks): Self::SystemData) {
    for (entity, attack) in (&*entities, &mut attacks).join() {
      if !attack.update() {
        entities.delete(entity).unwrap(); // we must call World::maintain() because of this.
      }
    }
  }
}

// pub trait AttackData : Send + Sync {
//   fn update(&mut self);
//   fn get_raw_dmg(&self) -> f32;
//   fn intersects_with(&self, bound: &Bound) -> bool;
//   fn get_sprite(&self, direction: Direction) -> Sprite;
// }
