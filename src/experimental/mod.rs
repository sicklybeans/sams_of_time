use specs::*;

use serde_json::{Value};

pub trait MyComponent : Component {
  fn load(json: &Value) -> Result<Self, String>;
}

// pub fn add_component<'a>(target: EntityBuilder<'a>, name: String, json: &Value) -> Result<EntityBuilder<'a>, String> {
//   match &name[..] {
//     "position" => {
//       Ok(target.with(Position::load(json).unwrap()))
//     },
//     _ => {
//       Err(format!("Could not load component '{}': unknown component name", name))
//     }
//   }
// }

// // pub fn get_component(typ: &str) -> dyn Component {
// //   if typ == "position" {
// //     Position(0.0, 0.0)
// //   } else {
// //     Movable{move_speed: 1.0}
// //   }
// // }

// pub fn main() {
//   let data = r#"
//   {
//     "position": {
//       "x": 1.5,
//       "y": 2.0
//     }
//   }
//   "#;
//   let result: Value = serde_json::from_str(data).unwrap();
//   Position::load(&result["position"]).unwrap();

//   println!("hi");
// }




