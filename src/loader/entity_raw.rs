use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct EntityListData {
  pub entities: Vec<String>
}

#[derive(Serialize, Deserialize)]
pub struct EntityData {
  pub name: String,
  pub actions: Vec<ActionData>,
  pub assets: AssetData,
  pub bounds: BoundData,
  pub max_speed: f32,
  pub rigid: bool,
}

#[derive(Serialize, Deserialize)]
pub struct ActionData {
  pub atype: String,
  pub animation: String,
  pub move_speed: f32,
  pub length: Option<u32>, // number of frames animation should last
  pub cooldown: Option<f32>,
  pub interruptable: bool,
}

#[derive(Serialize, Deserialize)]
pub struct AssetData {
  pub animations_file: String,
  pub sprites_file: String
}

#[derive(Serialize, Deserialize)]
pub struct BoundData {
  pub outer: SubBoundData,
  pub inner: Vec<SubBoundData>
}

#[derive(Serialize, Deserialize)]
pub struct SubBoundData {
  pub center_relative: (f32, f32),
  pub half_size: (f32, f32)
}
