use specs::prelude::*;
use Direction::*;

pub use event::*;
pub use experimental::*;
pub use crate::actions::components::*;
pub use crate::ai::components::*;
pub use crate::graphics::components::*;
pub use crate::physics::components::*;

pub fn get_world() -> World {
  let state = GameState{is_quit: false};
  let mut world = World::new();
  let dt: DeltaTime = 1.0/60.0;
  world.insert(dt);
  world.insert(state);
  world.insert(CollisionQueue { queue: Vec::new() });
  crate::actions::register_all(&mut world);
  crate::ai::register_all(&mut world);
  crate::graphics::register_all(&mut world);
  crate::physics::register_all(&mut world);
  world
}

pub type DeltaTime = f32;

pub struct Player {
  pub entity: Entity
}

pub struct GameState {
  pub is_quit: bool
}

#[derive(Copy, Clone)]
pub struct Dim {
  pub width: f32,
  pub height: f32
}

#[derive(Copy, Clone)]
pub struct Vector {
  pub x: f32,
  pub y: f32
}

pub type Point = Vector;

#[derive(Clone, Copy)]
pub enum Direction {
  Up = 0, Down = 1, Left = 2, Right = 3
}

impl PartialEq for Direction {
  fn eq(&self, other: &Self) -> bool {
    match (self, other) {
      (Up, Up) => true,
      (Down, Down) => true,
      (Left, Left) => true,
      (Right, Right) => true,
      _ => false
    }
  }
}

pub mod event {
  use super::*;

  /// Resource
  pub struct CollisionQueue {
    pub queue: Vec<CollisionEvent>
  }

  /// Event representing detection of a collision between the **outer** bounds of two entities with
  /// the bound component.
  pub struct CollisionEvent {
    pub entity1: Entity,
    pub entity2: Entity
  }

  impl CollisionQueue {
    pub fn clear(&mut self) {
      self.queue.clear();
    }
    pub fn queue(&mut self, entity1: Entity, entity2: Entity) {
      self.queue.push(CollisionEvent{entity1, entity2});
    }
  }
}

pub mod experimental {
  use super::*;

  pub fn register_all(world: &mut World) {
    world.register::<Health>();
  }

  pub struct Health {
    pub health: f32
  }

  impl Health {
    pub fn apply_dmg(&mut self, raw_dmg: f32) {
      println!("Damaged: {} -> {}", self.health, self.health - raw_dmg);
      self.health -= raw_dmg;
    }
  }

  impl Component for Health {
    type Storage = DenseVecStorage<Self>;
  }
}

impl From<(f32, f32)> for Point {
  fn from(tuple: (f32, f32)) -> Point {
    Point {
      x: tuple.0,
      y: tuple.1
    }
  }
}

impl From<(f32, f32)> for Dim {
  fn from(tuple: (f32, f32)) -> Dim {
    Dim {
      width: tuple.0,
      height: tuple.1
    }
  }
}
