use specs::prelude::*;

use crate::loader::LoadableEntity;
use crate::components::*;
use crate::game::Game;

pub fn main() {
  match demo_1() {
    Ok(_) => (),
    Err(msg) => {
      eprintln!("error: {}", msg);
      std::process::exit(1);
    }
  }
}

pub fn demo_1()  -> Result<(), String> {
  let mut game = Game::new()?;
  game.load_map("test1")?;

  let player_entity = game.world.create_entity()
    .load_from(&game.texture_manager, &game.entity_manager, "minman")?
    .with(Position::new(-4.5, 4.5))
    .with(Orientation::new(0.0, 1.0).unwrap())
    .build();
  game.world.insert(Player {entity: player_entity});

  game.world.create_entity()
    .load_from(&game.texture_manager, &game.entity_manager, "eyeball")?
    .with(Position::new(-4.5, -4.5))
    .with(Orientation::new(0.0, 1.0).unwrap())
    .build();

  // // Player that follows a script
  // game.world.create_entity()
  //   .load_from(&game.texture_manager, &game.entity_manager, "eyeball")?
  //   .with(Position::new(4.5, -4.5))
  //   .with(Orientation::new(-1.0, 0.0).unwrap())
  //   .with(AiWrapper::from(Demo1::new()))
  //   .with(ActionBuilder::new(&game.texture_manager)
  //     .add_default("male-default", true)
  //     .add(ActionType::Walk, "male-walk", ActionLength::Indefinite, true, true)
  //     .add(ActionType::Roll, "eyeball", ActionLength::Looped(60), true, false)
  //     .add(ActionType::Slash, "male-slash", ActionLength::Stretched(30), true, false)
  //     .build().unwrap())
  //   .with(game.texture_manager.get_sprite("male-walk-up-1").unwrap())
  //   .build();

  // // Player that follows a script
  // game.world.create_entity()
  //   .with(Position::new(0.0, -4.5))
  //   .with(Physics::new(2.0))
  //   .with(Orientation::new(0.0, -1.0).unwrap())
  //   .with(AiWrapper::from(RunTumble{k_tumble: 1.0}))
  //   .with(ActionBuilder::new(&game.texture_manager)
  //     .add_default("eyeball", true)
  //     .add(ActionType::Walk, "eyeball", ActionLength::Indefinite, true, true)
  //     .build().unwrap())
  //   .with(game.texture_manager.get_sprite("eyeball-down-1").unwrap())
  //   .build();

  game.run()
}


// pub fn animation_test()  -> Result<(), String> {
//   let mut game = Game::new()?;
//   game.load_map("test1")?;
//   add_random_objects(&mut game);

//   // add a player
//   let player_entity: PlayerEntity = game.world.create_entity()
//     .with(Player{})
//     .with(Position(0.0, 0.0))
//     .with(Velocity{vx: 0.0, vy: 0.0, mass: None})
//     .with(Movable{move_speed: 1.0})
//     .with(animation::get_male_animation(&game.texture_manager))
//     .with(game.texture_manager.get_sprite("male-up-animation-1")?)
//     .build();
//   game.world.insert(player_entity);

//   game.run()
// }

// pub fn map_test()  -> Result<(), String> {

//   let mut game = Game::new()?;
//   game.load_map("test1")?;

//   add_random_objects(&mut game);

//   // add a player
//   let player_entity: PlayerEntity = game.world.create_entity()
//     .with(Player{})
//     .with(Position(0.0, 0.0))
//     .with(Velocity{vx: 0.0, vy: 0.0, mass: None})
//     .with(Movable{move_speed: 1.0})
//     .with(game.texture_manager.get_sprite("male-up-animation-1")?)
//     .build();
//   game.world.insert(player_entity);

//   game.run()
// }


// // pub fn sprite_test()  -> Result<(), String> {
// //   let mut game = Game::new()?;

// //   // Elastic / Elastic
// //   game.world.create_entity()
// //     .with(Position( 8.0, -4.0))
// //     .with(Velocity{vx: -1.0, vy:  0.0, mass: None})
// //     .with(ColorRect::new(75, 20, 20, 1.2, 1.2))
// //     .with(Bounded{
// //       rigidity: Rigidity::Elastic(),
// //       outer_bound: collision::Rect{
// //         half_width: 0.6,
// //         half_height: 0.6
// //       }
// //     })
// //     .build();

// //   // add a player
// //   let player_entity: PlayerEntity = game.world.create_entity()
// //     .with(Player{})
// //     .with(Position(0.0, 0.0))
// //     .with(Velocity{vx: 0.0, vy: 0.0, mass: None})
// //     .with(Movable{move_speed: 1.0})
// //     .with(game.texture_manager.get_sprite("male-up-animation-1")?)
// //     .build();
// //   game.world.insert(player_entity);

// //   game.run()
// // }

// // pub fn collision_test() -> Result<(), String> {
// //   let mut game = Game::new()?;

// //   // add a player
// //   let player_entity: PlayerEntity = game.world.create_entity()
// //     .with(Player{})
// //     .with(Position(0.0, 0.0))
// //     .with(Velocity{vx: 0.0, vy: 0.0, mass: None})
// //     .with(Movable{move_speed: 1.0})
// //     .with(ColorRect::new(75, 75, 10, 0.8, 0.8))
// //     .build();
// //   game.world.insert(player_entity);

// //   // Elastic / Elastic
// //   game.world.create_entity()
// //     .with(Position( 8.0, -4.0))
// //     .with(Velocity{vx: -1.0, vy:  0.0, mass: None})
// //     .with(ColorRect::new(75, 20, 20, 1.2, 1.2))
// //     .with(Bounded{
// //       rigidity: Rigidity::Elastic(),
// //       outer_bound: collision::Rect{
// //         half_width: 0.6,
// //         half_height: 0.6
// //       }
// //     })
// //     .build();
// //   game.world.create_entity()
// //     .with(Position(-8.0, -4.0))
// //     .with(Velocity{vx:  0.5, vy:  0.0, mass: None})
// //     .with(ColorRect::new(20, 75, 20, 1.2, 1.2))
// //     .with(Bounded{
// //       rigidity: Rigidity::Elastic(),
// //       outer_bound: collision::Rect{
// //         half_width: 0.6,
// //         half_height: 0.6
// //       }
// //     })
// //     .build();

// //   // Inelastic / Inelastic
// //   game.world.create_entity()
// //     .with(Position( 8.0, -8.0))
// //     .with(Velocity{vx: -1.0, vy:  0.0, mass: None})
// //     .with(ColorRect::new(75, 20, 20, 1.2, 1.2))
// //     .with(Bounded{
// //       rigidity: Rigidity::Inelastic(),
// //       outer_bound: collision::Rect{
// //         half_width: 0.6,
// //         half_height: 0.6
// //       }
// //     })
// //     .build();
// //   game.world.create_entity()
// //     .with(Position(-8.0, -8.0))
// //     .with(Velocity{vx:  0.5, vy:  0.0, mass: None})
// //     .with(ColorRect::new(20, 75, 20, 1.2, 1.2))
// //     .with(Bounded{
// //       rigidity: Rigidity::Inelastic(),
// //       outer_bound: collision::Rect{
// //         half_width: 0.6,
// //         half_height: 0.6
// //       }
// //     })
// //     .build();

// //   // Elastic / Inelastic
// //   game.world.create_entity()
// //     .with(Position( 8.0,  4.0))
// //     .with(Velocity{vx: -1.0, vy:  0.0, mass: None})
// //     .with(ColorRect::new(75, 20, 20, 1.2, 1.2))
// //     .with(Bounded{
// //       rigidity: Rigidity::Inelastic(),
// //       outer_bound: collision::Rect{
// //         half_width: 0.6,
// //         half_height: 0.6
// //       }
// //     })
// //     .build();
// //   game.world.create_entity()
// //     .with(Position(-8.0,  4.0))
// //     .with(Velocity{vx:  0.5, vy:  0.0, mass: None})
// //     .with(ColorRect::new(20, 75, 20, 1.2, 1.2))
// //     .with(Bounded{
// //       rigidity: Rigidity::Elastic(),
// //       outer_bound: collision::Rect{
// //         half_width: 0.6,
// //         half_height: 0.6
// //       }
// //     })
// //     .build();

// //   game.run()
// // }

// pub fn add_obj(world: &mut World, x: f32, y: f32, vx: f32, vy: f32) {
//   world.create_entity()
//     .with(Position( x,  y))
//     .with(Velocity{vx: vx, vy: vy, mass: None})
//     .with(ColorRect::new(75, 20, 20, 0.4, 0.4))
//     // .with(Bounded{
//     //   rigidity: Rigidity::Elastic(),
//     //   outer_bound: collision::Rect{
//     //     half_width: 0.2,
//     //     half_height: 0.2
//     //   }
//     // })
//     .build();
// }

// fn add_random_objects(game: &mut Game) {
//   add_obj(&mut game.world, -8.0, -8.0, 0.6, 0.4);
//   add_obj(&mut game.world, -8.0, -4.0, 0.5, 1.0);
//   add_obj(&mut game.world, -8.0, 0.0, 0.9, 0.1);
//   add_obj(&mut game.world, -8.0, 4.0, -0.1, -0.3);
//   add_obj(&mut game.world, -8.0, 8.0, 0.8, 1.0);
//   add_obj(&mut game.world, -4.0, -8.0, -0.7, -0.2);
//   add_obj(&mut game.world, -4.0, -4.0, 0.9, 0.3);
//   add_obj(&mut game.world, -4.0, 0.0, -0.9, 0.8);
//   add_obj(&mut game.world, -4.0, 4.0, -0.5, -0.1);
//   add_obj(&mut game.world, -4.0, 8.0, 0.3, 0.3);
//   add_obj(&mut game.world, 0.0, -8.0, 0.9, 0.0);
//   add_obj(&mut game.world, 0.0, -4.0, -0.8, 0.9);
//   add_obj(&mut game.world, 0.0, 0.0, -0.7, 0.5);
//   add_obj(&mut game.world, 0.0, 4.0, 0.2, 0.7);
//   add_obj(&mut game.world, 0.0, 8.0, -0.9, 0.0);
//   add_obj(&mut game.world, 4.0, -8.0, 0.3, -0.6);
//   add_obj(&mut game.world, 4.0, -4.0, 0.7, 0.0);
//   add_obj(&mut game.world, 4.0, 0.0, 0.7, 0.5);
//   add_obj(&mut game.world, 4.0, 4.0, -0.5, 0.9);
//   add_obj(&mut game.world, 4.0, 8.0, 0.6, -0.4);
//   add_obj(&mut game.world, 8.0, -8.0, -0.9, 0.9);
//   add_obj(&mut game.world, 8.0, -4.0, -0.1, -0.7);
//   add_obj(&mut game.world, 8.0, 0.0, 0.2, -0.5);
//   add_obj(&mut game.world, 8.0, 4.0, -0.5, 1.0);
//   add_obj(&mut game.world, 8.0, 8.0, 0.3, -0.5);
// }

// pub fn main_test() -> Result<(), String> {
//   let mut game = Game::new()?;

//   // add_standard_background(&mut game.world);

//   // add a player
//   let player_entity: PlayerEntity = game.world.create_entity()
//     .with(Player{})
//     .with(Position(0.0, 0.0))
//     .with(Velocity{vx: 0.0, vy: 0.0, mass: None})
//     .with(Movable{move_speed: 1.0})
//     .with(ColorRect::new(75, 75, 10, 0.8, 0.8))
//     .build();
//   game.world.insert(player_entity);

//   // add some things
//   game.world.create_entity()
//     .with(Position(-8.0, -8.0))
//     .with(Velocity{vx: 0.5, vy: 1.0, mass: None})
//     .with(Movable{move_speed: 1.0})
//     .with(ColorRect::new(10, 10, 10, 2.0, 1.5))
//     .build();

//   // add some things
//   game.world.create_entity()
//     .with(Position(8.0, 8.0))
//     .with(Velocity{vx: 0.0, vy: 0.0, mass: None})
//     .with(Movable{move_speed: 0.75})
//     .with(ColorRect::new(10, 10, 50, 2.0, 2.0))
//     .with(Controlled::ChasePlayer(player_entity))
//     .build();

//   game.run()
// }
