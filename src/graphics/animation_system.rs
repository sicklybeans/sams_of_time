use specs::prelude::*;
use crate::components::*;

pub struct AnimationSystem;

impl<'a> System<'a> for AnimationSystem {
  type SystemData = (
    ReadStorage<'a, Action>,
    ReadStorage<'a, Orientation>,
    WriteStorage<'a, Sprite>
  );

  fn run(&mut self, (action, orientation, mut sprite): Self::SystemData) {
    for (action, orientation, sprite) in (&action, &orientation, &mut sprite).join() {
      *sprite = action.get_sprite(orientation.dir);
    }
  }
}
