pub mod animation_system;
mod renderer;
mod old_test;
pub mod components;
pub use renderer::render;

use specs::prelude::*;
use crate::components::*;

pub fn register_all(world: &mut World) {
  world.register::<Sprite>();
  world.register::<Animation>();
}

pub struct Region {
  pub p1: Point,
  pub p2: Point
}
