use specs::prelude::*;
use sdl2::render::WindowCanvas;
use sdl2::rect::Rect;
use sdl2::pixels::Color;

use crate::components::*;
use crate::game::Config;
use crate::map::Map;
use crate::resources::TextureManager;
use super::Region;

pub type SystemData<'a> = (
  ReadExpect<'a, Player>,
  ReadStorage<'a, Position>,
  ReadStorage<'a, Sprite>,
  ReadStorage<'a, Bound>,
);

pub fn render(
  config: &Config,
  canvas: &mut WindowCanvas,
  map: &Option<Map>,
  texture_manager: &TextureManager,
  (player, pos, sprite, bound): SystemData) -> Result<(), String> {
  canvas.set_draw_color(config.background_color);
  canvas.clear();

  let center = pos.get(player.entity)
    .expect("Couldnt find player position")
    .cur;

  let ppu = config.pixels_per_unit;
  let vp = canvas.viewport();

  // Determine the logical bounds of displayed image
  let logical_width = (vp.width() as f32) / ppu;
  let logical_height = (vp.height() as f32) / ppu;
  let logical_x1 = center.x - (logical_width/2.0);
  let logical_y1 = center.y - (logical_height/2.0);
  let scene_bounds = Region {
    p1: Point{
      x: logical_x1,
      y: logical_y1
    },
    p2: Point {
      x: logical_x1 + logical_width,
      y: logical_y1 + logical_height
    }
  };

  let transform_pt = |pt: &Point| -> (i32, i32) {
    (((pt.x - logical_x1) * ppu) as i32, ((pt.y - logical_y1) * ppu) as i32)
  };

  if let Some(map) = map {
    map.render(canvas, &config, scene_bounds)?;
  }

  // // Draw colored rectangle entities (stand in for sprites)
  // for (pos, color_rect) in (&pos, &color_rect).join() {
  //   // Todo: Maybe do some clipping?

  //   let cx = ((pos.0 - logical_x1) * ppu) as i32;
  //   let cy = ((pos.1 - logical_y1) * ppu) as i32;
  //   let cwidth = (color_rect.width * ppu) as u32;
  //   let cheight = (color_rect.height * ppu) as u32;
  //   canvas.set_draw_color(color_rect.color);
  //   canvas.fill_rect(Rect::from_center((cx, cy), cwidth, cheight))?
  // }

  // Draw sprites
  for (pos, sprite) in (&pos, &sprite).join() {
    let sprite_rect = sprite.region;
    let target_pt = transform_pt(&pos.cur);
    let screen_rect = Rect::from_center(target_pt, sprite_rect.width(), sprite_rect.height());
    canvas.copy(
      texture_manager.get_sprite_texture(sprite.texture_index),
      sprite_rect,
      screen_rect)?;
  }

  // Debugging feature: draw bounding rectangles
  for (pos, bound) in (&pos, &bound).join() {
    let (cx, cy) = transform_pt(&pos.cur);
    let (bw, bh) = (bound.outer.half_size.width, bound.outer.half_size.height);
    canvas.set_draw_color(Color::RGB(10, 115, 225));
    canvas.draw_rect(Rect::from_center((cx, cy), (2.0*ppu*bw) as u32, (2.0*ppu*bh) as u32))?;
    if let Some(inner) = &bound.inner {
      for sb in inner.iter() {
        let (cx, cy) = transform_pt(&(pos.cur + sb.center_relative));
        let (bw, bh) = (sb.half_size.width, sb.half_size.height);
        canvas.set_draw_color(Color::RGB(50, 115, 225));
        canvas.draw_rect(Rect::from_center((cx, cy), (2.0*ppu*bw) as u32, (2.0*ppu*bh) as u32))?;
      }
    }
  }

  canvas.present();
  Ok(())
}
