pub mod system;
pub mod components;

use specs::prelude::*;
pub use system::AiSystem;
pub use components::*;

pub fn register_all(world: &mut World) {
  world.register::<AiWrapper>();
}
