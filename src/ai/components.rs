use rand::Rng;
use rand::rngs::StdRng;
use specs::prelude::*;

use crate::components::*;

// General purpose AI wrapping code

pub trait Ai : Send + Sync {
  fn update(&mut self, rng: &mut StdRng, dt: f32, entity: Entity, pos: &Position, phy: &mut Physics, action: &mut Action, orientation: &mut Orientation);
}

pub struct AiWrapper {
  pub ai: Box<dyn Ai>
}

impl Component for AiWrapper {
  type Storage = DenseVecStorage<Self>;
}

impl AiWrapper {
  pub fn from<T: 'static +  Ai>(ai: T) -> AiWrapper {
    AiWrapper{ai: Box::new(ai)}
  }
  pub fn update(&mut self, rng: &mut StdRng, dt: f32, entity: Entity, pos: &Position, phy: &mut Physics, action: &mut Action, orientation: &mut Orientation) {
    self.ai.update(rng, dt, entity, pos, phy, action, orientation);
  }
}

// Specific AI profiles

pub struct RunTumble {
  pub k_tumble: f32
}

impl Ai for RunTumble {
  fn update(&mut self, rng: &mut StdRng, dt: f32, _entity: Entity, _pos: &Position, phy: &mut Physics, _action: &mut Action, orientation: &mut Orientation) {
    let r: f32 = rng.gen();
    let p_tumble = (dt) * (self.k_tumble);
    if r < p_tumble {
      let ang: f32 = rng.gen();
      *orientation = Orientation::from_angle(ang * 2.0 * 3.14159);
      phy.set_velocity(&*orientation, 1.0);
    }
  }
}

pub struct Demo1 {
  frame_counter: usize,
  rolled: bool,
  done: bool
}

impl Ai for Demo1 {
  fn update(&mut self, _rng: &mut StdRng, _dt: f32, _entity: Entity, pos: &Position, phy: &mut Physics, action: &mut Action, orientation: &mut Orientation) {
    if action.is_interruptable() {
      let speed_modifier = if self.frame_counter < 60 {
        *orientation = Orientation::from_angle(3.14159/2.0);
        action.set(ActionType::Walk)
      } else if !self.rolled {
        *orientation = Orientation::from_angle(3.14159/2.0);
        self.rolled = true;
        action.set(ActionType::Roll)
      } else if !self.done {
        if pos.cur.y < 4.5 {
          *orientation = Orientation::from_angle(3.14159/2.0);
          action.set(ActionType::Walk)
        } else if pos.cur.x > - 3.5 {
          *orientation = Orientation::from_angle(3.14159);
          action.set(ActionType::Walk)
        } else {
          *orientation = Orientation::from_angle(3.14159);
          self.done = true;
          action.set(ActionType::Slash)
        }
      } else {
        0.0
      };
      phy.set_velocity(&*orientation, speed_modifier);
    }
    self.frame_counter += 1;
  }
}

impl Demo1 {
  pub fn new() -> Demo1 {
    Demo1{frame_counter: 0, rolled: false, done: false}
  }
}
