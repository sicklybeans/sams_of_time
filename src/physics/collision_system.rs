use specs::prelude::*;
use crate::components::*;

pub struct CollisionDetectionSystem;

impl<'a> System<'a> for CollisionDetectionSystem {
  type SystemData = (
    WriteExpect<'a, CollisionQueue>,
    Entities<'a>,
    ReadStorage<'a, Position>,
    ReadStorage<'a, Bound>
  );
  fn run(&mut self, (mut collision_queue, ent, pos, bound): Self::SystemData) {

    let pos_list: Vec<(Entity, &Bound)> = (&*ent, &bound).join().collect();

    for (i, (e1, b1)) in pos_list.iter().enumerate() {
      'pair_loop: for (e2, b2) in pos_list[i+1..].iter() {
        let pos1 = pos.get(*e1)
          .expect(&format!("Entity has bound but no position component!"));
        let pos2 = pos.get(*e2)
          .expect(&format!("Entity has bound but no position component!"));

        if !pos1.moving_flag & !pos2.moving_flag {
          continue;
        }

        // 1. Compare outer bounds of both objects
        let pos_diff_nxt = pos1.nxt - pos2.nxt;
        let max_distance = b1.outer.half_size + b2.outer.half_size;
        if collide(pos_diff_nxt, max_distance) {
          collision_queue.queue(*e1, *e2);
        }
      }
    }
  }
}

#[inline(always)]
fn collide(pos_diff: Vector, maximum_distance: Dim) -> bool {
  return (pos_diff.x.abs() < maximum_distance.width) & (pos_diff.y.abs() < maximum_distance.height)
}
