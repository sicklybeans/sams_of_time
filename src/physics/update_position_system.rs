use specs::prelude::*;
use crate::components::*;
use super::collision_math::get_frame_fraction;

pub struct UpdatePositionSystem;

impl<'a> System<'a> for UpdatePositionSystem {
  type SystemData = (
    ReadExpect<'a, CollisionQueue>,
    WriteStorage<'a, Position>,
    ReadStorage<'a, Rigid>,
    ReadStorage<'a, Bound>
  );

  fn run(&mut self, (collision_queue, mut pos, rigid, bound): Self::SystemData) {

    // First handle all possible collisions by:
    //  1. determining if they occured between rigid objects
    //  2. determining if their was actually a collision (not just outer bound collision)
    //  3. determining what portion of proposed move may be implemented without collision occurring.
    'event_loop: for event in collision_queue.queue.iter() {
      let (e1, e2) = (event.entity1, event.entity2);

      // All collisions require Position and Bound components so these lines should never fail.
      let pos1 = pos.get(e1).expect("Serious bug: collision event entity missing position component");
      let pos2 = pos.get(e2).expect("Serious bug: collision event entity missing position component");
      let b1 = bound.get(e1).expect("Serious bug: collision event entity missing bound component");
      let b2 = bound.get(e2).expect("Serious bug: collision event entity missing bound component");

      // Only some collisions have Rigid component (indicating that these entities cannot pass)
      let (rig1, rig2) = (rigid.get(e1), rigid.get(e2));

      // Flag indicating if collision truly occurred.
      let mut collision = false;

      // number between 0.0 and 1.0 indicating what fraction of proposed move we can take without a collision
      let mut min_required_rollback= 1.0;
      match (rig1, rig2) {
        (Some(Rigid), Some(Rigid)) => {

          let d_pos_i = pos1.cur - pos2.cur;
          let entity_distance_final = pos1.nxt - pos2.nxt;
          let entity_distance_change = entity_distance_final - d_pos_i;

          // check for collision for each sub bounding rectangle.
          for sb1 in b1.subbounds_or_outer().iter() {
            for sb2 in b2.subbounds_or_outer().iter() {
              let distance = entity_distance_final + sb1.center_relative - sb2.center_relative;
              let collision_distance = sb1.half_size + sb2.half_size;

              if (distance.x.abs() >= collision_distance.width) | (distance.y.abs() >= collision_distance.height) {
                continue
              }

              collision = true;

              // figure out what fraction of frame we can implement without collision. If this number
              // is less than zero then it indicates that the objects collided even in the previous
              // frame.
              if let Some(f) = get_frame_fraction(entity_distance_change, distance, collision_distance) {
                if f < min_required_rollback {
                  min_required_rollback = f;
                }
              } else {
                continue 'event_loop;
              }
            }
          }
        },
        (_, _) => {}
      }

      // assert!(0.0 < min_required_rollback < 1.0)

      // If collision truly occurred, "roll it back" by implementing maximum portion of frame such
      // that no collision occurs.
      if collision {
        let mut p1 = pos.get_mut(e1).unwrap();
        p1.nxt = p1.cur + (p1.nxt - p1.cur) * min_required_rollback;
        let mut p2 = pos.get_mut(e2).unwrap();
        p2.nxt = p2.cur + (p2.nxt - p2.cur) * min_required_rollback;
      }
    }

    // Increment steps
    for pos in (&mut pos).join() {
      pos.cur = pos.nxt;
      pos.moving_flag = false;
    }
  }
}

