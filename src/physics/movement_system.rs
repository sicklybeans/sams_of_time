use specs::prelude::*;
use crate::components::*;

/// System which proposes moves and marks objects as either moving in this frame or not
pub struct MovementSystem;

impl<'a> System<'a> for MovementSystem {
  type SystemData = (
    ReadExpect<'a, DeltaTime>,
    WriteStorage<'a, Position>,
    ReadStorage<'a, Physics>,
  );

  fn run(&mut self, (dt, mut pos, phy): Self::SystemData) {
    let dt = *dt;
    for (pos, phy) in (&mut pos, &phy).join() {
      pos.nxt = pos.cur + phy.vel * dt;
      pos.moving_flag = true; // used to make collision detection simpler by ignoring pairs of unmoved objects.
    }
  }
}
