use crate::components::*;


/// Determines if n-gon intersections rectangle bound.
///
/// This method is slowish so should only be called for a small number of entities each frame.
pub fn get_collision_ngon_rect(ngon: &Vec<Point>, rect: SubBound) -> bool {
  todo!();
}




/// Returns fraction of proposed move that can be implemented without a collision occurring.
///
/// # Arguments
/// * `distance_change` - Change in distance between the center of two entities between the
///   proposed move and the last frame.
/// * `distance_final` - Distance between the center of two entities after the proposed move.
/// * `collision_distance` - Minimum separation between center of two entities before they are
///   considered to be in collision.
///
/// Return value guarantees:
/// Some(f): 0.0 < f < 1.0: collision can be prevented by applying `f` fraction of proposed move
/// None: collision occurred in previous frame, cannot be prevented
#[inline(always)]
pub fn get_frame_fraction(
  distance_change: Vector,
  distance_final: Vector,
  collision_distance: Dim) -> Option<f32> {
  // If `entity_distance_change` and `sb_distance_final` have opposite signs, then the objects were
  // getting closer to colliding. Otherwise, the objects were moving away from one another.
  // It's possible that the objects were moving towards one another in one direction and away from
  // one another in the other direction.
  // This computes the value dt < 1.0 necessary to prevent a collision where dt = 1.0 implies the
  // current proposed move. If dt < 0.0, it implies that the objects collided in this axis even in
  // the previous frame.
  let mut max_required_rollback = 0.0;
  let rollback_time_x = get_frame_fraction_axis(distance_final.x, distance_change.x, collision_distance.width);
  let rollback_time_y = get_frame_fraction_axis(distance_final.y, distance_change.y, collision_distance.height);

  // If both rollback times are None, it indicates that the collision also happened in the previous
  // frame and thus there is no way to correctly undo the collision.
  if let (None, None) = (rollback_time_x, rollback_time_y) {
    None
  } else {
    if let Some(dt_x) = rollback_time_x {
      if dt_x > max_required_rollback {
        max_required_rollback = dt_x;
      }
    }
    if let Some(dt_y) = rollback_time_y {
      if dt_y > max_required_rollback {
        max_required_rollback = dt_y;
      }
    }
    Some(max_required_rollback)
  }
}

/// Return value guarantees:
/// Some(f): 0.0 < f < 1.0: collision can be prevented by applying `f` fraction of proposed move
/// Some(f): f < 0.0      : collision occurred in previous frame, cannot be prevented
/// None                  : cannot be prevented, objects moving away from each other.
#[inline(always)]
fn get_frame_fraction_axis(
  obj_distance: f32,
  obj_distance_change: f32,
  collision_distance: f32) -> Option<f32> {
  if (obj_distance > 0.0) & (obj_distance_change < 0.0) {
    Some(1.0 + (collision_distance - obj_distance)/obj_distance_change)
  } else if (obj_distance < 0.0) & (obj_distance_change > 0.0) {
    Some(1.0 - (collision_distance + obj_distance)/obj_distance_change)
  } else {
    None
  }
}