import json
import os
from typing import List, Dict, Optional, Any

from resource_loader import ResourceManager, Sprite

class Map():
  _tile_map: Dict[int, Sprite]
  _background_layer: List[List[int]]
  _x1: float
  _y1: float
  _rm: ResourceManager

  def __init__(self, x1: float, y1: float, tile_map: Dict[int, str], bg: List[List[int]]) -> None:
    self._rm = ResourceManager.get()
    self._x1 = x1
    self._y1 = y1
    self._tile_map = {
      id, self._rm.get_sprite(key) for id, key in tile_map.items()
    }
    self._background_layer = bg
    self._check_integrity()

  def _check_integrity(self) -> None:
    nrows = len(self._background_layer)
    ncols = len(self._background_layer[0])
    for i, row in enumerate(self._background_layer):
      if len(row) != ncols:
        raise Exception('Row %d has different length %d != %d' % (i, len(row), ncols))

    for i in range(nrows):
      for j in range(ncols):
        if self._background_layer[i][j] not in self._tile_map:
          raise Exception('Tile id not in key: %d' % self._background_layer[i][j])

    sprite_width, sprite_height = None, None
    for id, sprite in self._tile_map.items():
      if sprite_width is None:
        sprite_width, sprite_height = sprite.get_size()
      else:
        w, h = sprite.get_size()
        if w != sprite_width:
          raise Exception('Map contains uneven tile sizes')
        if h != sprite_height:
          raise Exception('Map contains uneven tile sizes')

  def get_pixel_size(self):
    sw, sh = self._tile_map[0].get_size()
    nrows = len(self._background_layer)
    ncols = len(self._background_layer[0])
    return sw*ncols, sh*nrows

  def get_image(self) -> GdkPixbuf.Pixbuf:
    w, h = self.get_pixel_size()
    pb = GdkPixbuf.Pixbuf.new(Gdk.ColorSpace.RGB, True, 8, w, h)
    sprite_w, sprite_h = self._tile_map[0].get_size()
    nrows = len(self._background_layer)
    ncols = len(self._background_layer[0])
    for i in range(nrows):
      for j in range(ncols):



  @classmethod
  def _from_json(cls, d) -> Map:
    tile_map = {
      id: key for id, key in d['tile_key']
    }
    return Map(d['x1'], d['y1'], tile_map, d['background_layer'])

  @classmethod
  def load(cls, name: str) -> Map:
    d = json.loads(open(os.path.join('../assets/levels', name, 'map.json')).read())
    return Map._from_json(d)

if __name__ == '__main__':
  m = Map.load('test1')
  print(m._x1)