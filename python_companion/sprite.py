from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

from typing import Tuple

EMPTY_KEY = 'empty'

class Sprite():
  key: str
  width: int
  height: int
  image: GdkPixbuf.Pixbuf

  def __init__(self, key: str, image: GdkPixbuf.Pixbuf) -> None:
    self.key = key
    self.image = image
    self.width = image.get_width()
    self.height = image.get_height()

  def get_size(self) -> Tuple[int, int]:
    return self.width, self.height

  def get_image(self) -> GdkPixbuf.Pixbuf:
    return self.image

  def draw_at(self, dst: GdkPixbuf.Pixbuf, x: int, y: int) -> None:
    self.image.copy_area(0, 0, self.width, self.height, dst, x, y)
