
LOGICAL_PIXEL_SIZE = 1.0 / 32.0 # 1 in game unit = 32 pixels

class Error(Exception):
  msg: str
  def __init__(self, msg: str) -> None:
    super().__init__(msg)
    self.msg = msg


class MissingResourceError(Error):
  pass

class ParseException(Error):
  pass
