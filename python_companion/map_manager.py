from __future__ import annotations

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

from typing import Dict, List, cast

from core import MissingResourceError
from map import Map
from map_loader import RData, RLevelData
from sprite_manager import SpriteManager


class MapManager():
  maps: Dict[str, Map]
  sprite_manager: SpriteManager

  def __init__(self, sprite_manager: SpriteManager) -> None:
    self.maps = {}
    self.sprite_manager = sprite_manager
    self._add_resource_dir('../assets/levels')

  def list_maps(self) -> List[str]:
    return list(self.maps.keys())

  def get(self, key: str) -> Map:
    if key in self.maps:
      return self.maps[key]
    else:
      raise MissingResourceError('No such map: %s' % key)

  def _add_resource_dir(self, resource_dir: str) -> None:
    RLevelData.load_resource(resource_dir).consume(self.sprite_manager, self.maps)

  def report_info(self) -> None:
    print('MapManager has %d levels' % len(self.maps))

if __name__ == '__main__':
  sm = SpriteManager()
  mm = MapManager(sm)
  mm.report_info()
