import json, os
from typing import ClassVar, Type, TypeVar, Any, Dict

from core import MissingResourceError

ConcreteLoader = TypeVar('ConcreteLoader', bound='ResourceLoader')  # cant use forward reference here

class ResourceLoader():
  JSON_FILE: ClassVar[str] = 'info.json'
  resource_dir: str
  _consumed: bool

  def __init__(self, resource_dir: str, json_dict: Dict[str, Any]) -> None:
    self.resource_dir = resource_dir
    self._consumed = False

  def __repr__(self) -> str:
    return self.resource_dir

  @classmethod
  def load_resource(cls: Type[ConcreteLoader], resource_dir: str) -> ConcreteLoader:
    resource_file = os.path.join(resource_dir, cls.JSON_FILE)
    if not os.path.exists(resource_file):
      raise MissingResourceError('Missing resource file "%s"' % resource_file)
    return cls(resource_dir, json.loads(open(resource_file).read()))

  def check_consume(self) -> None:
    if self._consumed:
      raise Exception('Resource "%s" has already been consumed' % self)
    self._consumed = True

