def prn(c1):
  print('(%3d, %3d, %3d)' % (c1[0], c1[1], c1[2]))

def sub(c1, c2):
  return (c1[0] - c2[0], c1[1] - c2[1], c1[2] - c2[2])

def add(c1, c2):
  return (c1[0] + c2[0], c1[1] + c2[1], c1[2] + c2[2])

def grad(c1, c2, num):
  diff = sub(c2, c1)
  div = num+1
  parts = (diff[0]/div, diff[1]/div, diff[2]/div)
  return [
    c1,
    add(c1, parts),
    add(add(c1, parts), parts),
    c2
  ]

c1 = (155, 173, 183)
c2 = (132, 126, 135)
for c in grad(c1, c2, 2):
  prn(c)
