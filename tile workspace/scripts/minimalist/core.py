import enum
from typing import Tuple


CHAR_SIZE = 64
PIXELS_PER_UNIT = 32.0

Pixel = Tuple[int, int]
Rect = Tuple[Pixel, int, int]
Color = Tuple[int, int, int]

class Direc(enum.Enum):
  E = 0
  SE = 1
  S = 2
  SW = 3
  W = 4
  NW = 5
  N = 6
  NE = 7

  def __repr__(self) -> str:
    if self == Direc.E:
      return 'e'
    elif self == Direc.S:
      return 's'
    elif self == Direc.N:
      return 'n'
    elif self == Direc.W:
      return 'w'
    elif self == Direc.SE:
      return 'se'
    elif self == Direc.NE:
      return 'ne'
    elif self == Direc.SW:
      return 'sw'
    elif self == Direc.NW:
      return 'nw'
    else:
      raise Exception()

  def __str__(self) -> str:
    return repr(self)
