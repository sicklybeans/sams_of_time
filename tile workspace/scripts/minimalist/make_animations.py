import warnings, itertools, sys, enum
import numpy as np
from typing import List, Tuple
from PIL import Image
from PIL.ImageDraw import Draw

from core import *

NUM_FRAMES = 5
ARROW_BODY_FRACTION = 0.625
FRAME_COLOR = (251, 242, 54)
OUTLINE_COLOR = (10, 10, 10)

WALK_COLOR = (48, 96, 130)
ROLL_COLOR = (95, 205, 228)
ATTK_COLOR = (217, 87, 99)
BASE_COLOR = (238, 195, 154)

OUTLINE = True
HEAD_RECT = ((27, 15), 10, 10) # x1, y1, width, height
BODY_RECT = ((16, 25), 32, 32)


def get_directional_rectangle(body: Rect, frac: float=ARROW_BODY_FRACTION) -> Rect:
  if body[1] % 2 != 0 or body[2] % 2 != 0:
    raise Exception('Body dimensions must be even')
  width = 2 * int(0.5 * frac * body[1])
  height = 2 * int(0.5 * frac * body[1])
  x1 = body[0][0] + int((body[1] - width)/2)
  y1 = body[0][1] + int((body[2] - height)/2)
  return ((x1, y1), width, height)

def draw_arrow(img: Image, rect: Rect, c: Color, offset: Pixel, dir: Direc) -> None:
  dr = Draw(img)
  x1, y1 = rect[0][0] + offset[0], rect[0][1] + offset[1]
  x2, y2 = x1 + rect[1] - 1, y1 + rect[2] - 1
  xh1 = x1 + int(rect[1]/2) - 1
  xh2 = xh1 + 1
  yh1 = y1 + int(rect[2]/2) - 1
  yh2 = yh1 + 1

  if dir == Direc.E or dir == Direc.W:
    dr.rectangle([(x1, yh1), (x2, yh2)], fill=c)
    if dir == Direc.E:
      dr.line([(x2-4, yh1-4), (x2, yh1)], fill=c)
      dr.line([(x2-4, yh2+4), (x2, yh2)], fill=c)
    if dir == Direc.W:
      dr.line([(x1, yh1), (x1+4, yh1-4)], fill=c)
      dr.line([(x1, yh2), (x1+4, yh2+4)], fill=c)
    return
  elif dir == Direc.S or dir == Direc.N:
    x1, x2 = xh1, xh2
    dr.rectangle([(x1, y1), (x2, y2)], fill=c)
    if dir == Direc.S:
      dr.line([(x1-4, y2-4), (x1, y2)], fill=c)
      dr.line([(x2, y2), (x2+4, y2-4)], fill=c)
    if dir == Direc.N:
      dr.line([(x1, y1), (x1-4, y1+4)], fill=c)
      dr.line([(x2, y1), (x2+4, y1+4)], fill=c)
    return
  elif dir == Direc.SE or dir == Direc.NW:
    dr.line([(x1, y1), (x2, y2)], fill=c)
    dr.line([(x1, y1+1), (x2-1, y2), (x2, y2-1), (x1+1, y1)], fill=c)
    if dir == Direc.NW:
      dr.line([(x1, y1+6), (x1, y1)], fill=c)
      dr.line([(x1, y1), (x1+6, y1)], fill=c)
    if dir == Direc.SE:
      dr.line([(x2-6, y2), (x2, y2)], fill=c)
      dr.line([(x2, y2-6), (x2, y2)], fill=c)
  elif dir == Direc.SW or dir == Direc.NE:
    dr.line([(x1, y2), (x2, y1)], fill=c)
    dr.line([(x1, y2-1), (x2-1, y1), (x2, y1+1), (x1+1, y2)], fill=c)
    if dir == Direc.SW:
      dr.line([(x1, y2-6), (x1, y2)], fill=c)
      dr.line([(x1, y2), (x1+6, y2)], fill=c)
    if dir == Direc.NE:
      dr.line([(x2-6, y1), (x2, y1)], fill=c)
      dr.line([(x2, y1), (x2, y1+6)], fill=c)

def draw_outline(img: Image, rect: Rect, c: Color, offset: Pixel) -> None:
  x1 = offset[0] + rect[0][0] - 1
  x2 = offset[0] + rect[0][0] + rect[1] + 1
  y1 = offset[1] + rect[0][1] - 1
  y2 = offset[1] + rect[0][1] + rect[2] + 1
  for i in range(y1, y2):
    img.putpixel((x1, i), c)
    img.putpixel((x2-1, i), c)
  for j in range(x1, x2):
    img.putpixel((j, y1), c)
    img.putpixel((j, y2-1), c)

def draw_rect(img: Image, r: Rect, c: Color, offset: Pixel) -> None:
  for i in range(r[2]):
    for j in range(r[1]):
      img.putpixel((offset[0] + r[0][0] + j, offset[1] + r[0][1] + i), c)

def draw_frame_bar(img: Image, head_rect: Rect, c: Color, offset: Pixel, step_size: int, frame: int) -> None:
  height = head_rect[2]
  x = offset[0] + head_rect[0][0] + step_size*frame
  y = offset[1] + head_rect[0][1]
  for i in range(height):
    for j in range(step_size):
      img.putpixel((x+j, y+i), c)

def draw_base(img: Image, body: Rect, head: Rect, c: Color, offset: Pixel) -> None:
  if OUTLINE:
    draw_outline(img, body, OUTLINE_COLOR, offset)
    draw_outline(img, head, OUTLINE_COLOR, offset)
  draw_rect(img, body, c, offset)
  draw_rect(img, head, c, offset)

def make_image(filename: str, body: Rect, head: Rect, c: Color, num_frames: int=5) -> None:
  # head with must be divisible by number of frames
  if head[1] % num_frames != 0:
    raise Exception('Width of head must be divisible by number of frames')
  step_size = int(head[1] / num_frames)

  dir_rect = get_directional_rectangle(body)

  img = Image.new('RGBA', (CHAR_SIZE*num_frames, CHAR_SIZE*len(Direc)))

  for dir in Direc:
    for j in range(num_frames):
      x, y = j * CHAR_SIZE, dir.value * CHAR_SIZE
      draw_base(img, body, head, c, (x, y))
      draw_frame_bar(img, head, FRAME_COLOR, (x,y), step_size, j)
      draw_arrow(img, dir_rect, OUTLINE_COLOR, (x, y), dir)
  img.save(filename)

