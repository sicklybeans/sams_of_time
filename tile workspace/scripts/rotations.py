import numpy as np
from PIL import Image

def rotate(pt, ang):
  r = np.array([[np.cos(ang), -np.sin(ang)], [np.sin(ang), np.cos(ang)]])
  return np.matmul(r, pt)

class Pt():
  def __init__(self, x, y):
    self.x = x
    self.y = y

class Quad():
  def __init__(self, p1, p2, p3, p4):
    self.pts = [p1, p2, p3, p4]

  @staticmethod
  def from_corners(p1, p2):
    return Quad(
      np.array([p1.x, p1.y]),
      np.array([p1.x, p2.y]),
      np.array([p2.x, p2.y]),
      np.array([p2.x, p1.y]))

  def rotate(self, ang):
    r = np.array([[np.cos(ang), -np.sin(ang)], [np.sin(ang), np.cos(ang)]])
    pts = [np.matmul(r, pt) for pt in self.pts]
    return Quad(pts[0], pts[1], pts[2], pts[3])

def draw_quad(name, quad, offset_x=15.5, offset_y=15.5):
  offset = np.array([offset_x, offset_y])
  pts = [pt + offset for pt in quad.pts]

  img = Image.new('RGB', (32, 32), color='white')

  for pt in pts:
    x = pt[0]
    y = pt[1]
    xi = int(np.round(x))
    yi = int(np.round(y))
    img.putpixel([xi, yi], (0, 0, 0))

  img.save(name)

rect = Quad.from_corners(Pt(-3.0, -15.5), Pt(3.0, 15.5))
draw_quad('theta0.png', rect)
draw_quad('theta1.png', rect.rotate(np.pi/8))
draw_quad('theta2.png', rect.rotate(2*np.pi/8))
draw_quad('theta3.png', rect.rotate(3*np.pi/8))
draw_quad('theta4.png', rect.rotate(4*np.pi/8))

# img = Image.new('RGB', (32, 32), color='white')



